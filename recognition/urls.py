from django.urls import path
from . import views
from django.views.decorators.csrf import csrf_exempt

app_name = 'recognition'
urlpatterns = [
    path('recognition', csrf_exempt(views.index), name='index'),
    path('upload', csrf_exempt(views.upload), name='upload'),
]
