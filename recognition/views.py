import cv2
import os
import numpy as np
import face_recognition
from .models import Attendance
from datetime import datetime
from django.http import HttpResponse
from django.core.files.storage import FileSystemStorage


def findEncodings(images):
    encodeList = []

    for img in images:
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        encode = face_recognition.face_encodings(img)[0]
        encodeList.append(encode)
    return encodeList


def markAttendance(name):
    data = Attendance(name=name, date=datetime.now())
    data.save()


def index(request):
    upload_image = request.FILES['image']

    path = 'images_basic'
    images = []
    classNames = []
    myList = os.listdir(path)
    for cl in myList:
        curImg = cv2.imread(f'{path}/{cl}')
        images.append(curImg)
        classNames.append(os.path.splitext(cl)[0])

    encodeListKnown = findEncodings(images)

    inputImage = face_recognition.load_image_file(upload_image)
    inputImage = cv2.cvtColor(inputImage, cv2.COLOR_BGR2RGB)

    faceLoc = face_recognition.face_locations(inputImage)[0]
    encodeElon = face_recognition.face_encodings(inputImage)[0]
    cv2.rectangle(inputImage, (faceLoc[3], faceLoc[0]), (faceLoc[1], faceLoc[2]), (255, 0, 255), 2)

    results = face_recognition.compare_faces(encodeListKnown, encodeElon)
    faceDis = face_recognition.face_distance(encodeListKnown, encodeElon)

    matchIndex = np.argmin(faceDis)

    if results[matchIndex]:
        name = classNames[matchIndex].upper()
        markAttendance(name)
        return HttpResponse(name + " attendance is made")
    else:
        return HttpResponse("Not matched")


def upload(request):
    upload_image = request.FILES['image']
    fs = FileSystemStorage('images_basic')
    filename = fs.save(request.POST.get('name') + '.jpg', upload_image)
    path = fs.path(filename)
    return HttpResponse(path)